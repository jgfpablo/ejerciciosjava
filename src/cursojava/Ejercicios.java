/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cursojava;

import java.util.Scanner;

/**
 *
 * @author PabloFrank
 */
public class Ejercicios {
    
    Scanner scan = new Scanner(System.in);
    
   
    
    
   
    
     public static void HolaMundo(){
        System.out.println("Hola Mundo");
    }
     
      public void MostrarNombre(){
        System.out.println("Escribe tu nombre");
        String nombre = scan.next();
        System.out.println("Hola "+ nombre);
    }
    
    
    public  void EjerciciosMatematicos(){
        // Las variables son double pero se usa la función nextInt()
        System.out.println("Escriba el primer numero");
        double n1 = scan.nextInt();
        System.out.println("Escriba el segundo numero");
        double n2 = scan.nextInt();
        
        
        double suma = n1+n2;
        double resta = n1-n2;
        double multiplicacion = n1*n2;
        double division = n1/n2;
        
        System.out.println("Los resultados son los siguientes:");
        System.out.println("Suma: "+suma);
        System.out.println("Resta: "+resta);
        System.out.println("Multiplicacion: "+multiplicacion);
        System.out.println("Division: "+division);
        
    }
    
    public void PromedioAlturas(){
        System.out.println("Escribe la primer estatura");
        double estatura1 = scan.nextDouble();
        System.out.println("Escribe la segunda estatura");
        double estatura2 = scan.nextDouble();
        System.out.println("Escribe la tercer estatura");
        double estatura3 = scan.nextDouble();
        
        double promedio = (estatura1+estatura2+estatura3) / 3;
        
        String promedioFormateado = String.format("%.2f", promedio);
        
        System.out.println("El promedio de las tres estaturas es : "+promedioFormateado);
        
    }
    
    
    public void AreaPerimetro(){
        
        System.out.println("Ingrese el valor del radio de la circunferencia");
        double radio = scan.nextDouble();
        
        double area = Math.PI * Math.pow(radio, 2);
        double perimetro = 2 * Math.PI * radio;
        
        System.out.println("el area es: "+area);
        System.out.println("el perimetro es: "+perimetro);
        
    }
    
    public void Descuentos(){
        System.out.println("Ingrese el precio de un producto");
        double precio = scan.nextDouble();
        System.out.println("Ingrese el % de descuento");
        double descuento = scan.nextDouble();
        
        double cantidadDescuento = precio*descuento/100;
        
        System.out.println("La cantidad del descuento es : "+ cantidadDescuento+"$");
        System.out.println("El precio a pagar con el descuento es : "+(precio-cantidadDescuento)+"$");
    }
    
    public void IntercambioValores(){
        System.out.println("Ingrese la edad de la persona 1");
        int Persona1 = scan.nextInt();
        System.out.println("Ingrese la edad de la persona 2");
        int Persona2 = scan.nextInt();
        
        int aux = Persona2;
        
        Persona2 = Persona1;
        Persona1 = aux;
        
        System.out.println("La edad de la persona 1 es : "+Persona1);
                System.out.println("La edad de la persona 2 es : "+Persona2);

    }
    
    public void Temperaturas(){
        System.out.println("Ingrese una Temperatura Celcius");
        double Celsius = scan.nextDouble();
        
        double Kelvin = 273.15 + Celsius ;
        
        double Fahrenheit = 1.8 * Celsius;

        System.out.println("La Temperatura en grados Kelvin es : "+Kelvin);
        System.out.println("La temperatura en grados Fahrenheit es :"+ Fahrenheit);
        
    }
    
    
    public void Convertidor(){
        
        System.out.println("Ingrese una cantidad de pesos $");
       double peso = scan.nextDouble();
       
        
double dolar = 231.68 ;
double euro = 250.69 ;
double guaranies = 31.00 ;
double real = 46.81 ;

        // La fórmula para convertir a guaraníes está mal
        System.out.println("La cantidad de dolares obtenidos es :"+(peso/dolar));
                System.out.println("La cantidad de euros obtenidos es :"+(peso/euro));
                        System.out.println("La cantidad de guaranies obtenidos es :"+(peso/guaranies));
                           System.out.println("La cantidad de reales obtenidos es :" +(peso/real));
    }
}
